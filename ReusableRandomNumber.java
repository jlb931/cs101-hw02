/*
 * Author: Jack Booth
 * Date: 9/28/16
 * 
 * Generates a random number between 0 and 100 until 50 is reached while counting the number of loops it takes too reach 50.
 */
public class ReusableRandomNumber
{
	public static void main(String[] args)
	{
//		variable declarations
		int test = 0;
		int count = 0;
		
//		runs generate method until 50 is generated while continusously counting the number of loops.
		while(test != 50)
		{
			test = generate(0, 100);
			System.out.println(test);
			count++;
		}
		System.out.println("it took " + count +" tries to get to 50.");
	}
	
	/**
	 * Generates a random number within a range
	 * @param min is the minimum end of the range
	 * @param max is the maximum end of the range
	 * @return the random number as an int
	 */
	public static int generate(int min, int max)
	{
		int range = (max -min) + 1;
		int ret = min + (int)(Math.random()*range);
		return ret;
	}
}
