/*
 * Author: Jack Booth
 * Date: 9/28/16
 * 
 * Tests if an input string is a palindrome.
 */
import java.util.Scanner;

public class Palindrome
{
	public static void main(String[] args)
	{
//		Take user input
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a word to check if it is a palindrome");
		String input = in.nextLine();
		
//		Checks if input is a palindrome using the below methods, then prints the result.
		if(check(input, reverse(input)) == true)
			System.out.println(input + " is a palindrome.");
		else
			System.out.println(input + " is not a palindrome.");
		in.close();
	}
	
	/**
	 * Reverses a word
	 * @param word is the word to reverse
	 * @return the reverse of the input word
	 */
	public static String reverse(String word)
	{
		String ret = "";
		for(int i = word.length() - 1; i >= 0; i--)
		{
			ret += word.charAt(i);
		}
		return ret;
	}
	
	/**
	 * Checks if two words are equal.
	 * @param original the original word
	 * @param reverse the reverse of the word
	 * @return true if and only if the original and reverse are equal to each other
	 */
	public static boolean check(String original, String reverse)
	{
		boolean check = false;
		if(original.contentEquals(reverse))
			check = true;
		return check;
	}
}
